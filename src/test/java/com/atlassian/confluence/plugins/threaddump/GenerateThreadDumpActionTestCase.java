package com.atlassian.confluence.plugins.threaddump;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import junit.framework.TestCase;

public class GenerateThreadDumpActionTestCase extends TestCase
{
    private GenerateThreadDumpAction generateThreadDumpAction;
    
    @Mock private ThreadDumpBuilder threadDumpBuilder;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        generateThreadDumpAction = new GenerateThreadDumpAction();
        generateThreadDumpAction.setThreadDumpBuilder(threadDumpBuilder);
    }

    public void testGenerateThreadDumpOutput() throws Exception
    {
        String threadDumpBuilderMessage = "Testing ThreadDumpBuilder Message";
        
        when(threadDumpBuilder.build()).thenReturn(threadDumpBuilderMessage);
        
        assertEquals("success", generateThreadDumpAction.execute());
        assertEquals(threadDumpBuilderMessage, generateThreadDumpAction.getThreadDumpOutput());
    }
}
