package com.atlassian.confluence.plugins.threaddump;

import com.atlassian.confluence.util.GeneralUtil;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ThreadDumpBuilderTestCase extends TestCase
{
    private ThreadDumpBuilder threadDumpBuilder;
    
    private Map<Thread, StackTraceElement[]> allStackTraces;

    @Mock
    private Thread currentThread;

    @Mock Thread otherThread;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);

        allStackTraces = new HashMap<Thread, StackTraceElement[]>();
        allStackTraces.put(
                currentThread,
                new StackTraceElement[] {
                        new StackTraceElement(getClass().getName(), "setUp", "ThreadDumpBuilderTestCase.java", 1)
                }
        );
        allStackTraces.put(
                otherThread, 
                new StackTraceElement[] {
                        new StackTraceElement(getClass().getName(), "setUp", "ThreadDumpBuilderTestCase.java", 2)
                }
        );
        
        
        threadDumpBuilder = new ThreadDumpBuilder()
        {
            @Override
            protected Set<Entry<Thread, StackTraceElement[]>> getAllStackTracesEntrySet()
            {
                return allStackTraces.entrySet();
            }

            @Override
            Thread getCurrentThread()
            {
                return currentThread;
            }
        };
    }

    public void testThreadDumpBuiltFromAllLiveThreadsExceptTheCurrentOne()
    {
        StringBuilder expectedOutput = new StringBuilder();
        
        appendPrefix(expectedOutput);
        expectedOutput.append(otherThread).append("\n");
        for (StackTraceElement element : allStackTraces.get(otherThread))
            expectedOutput.append("\t").append(element).append("\n");
        
        assertEquals(expectedOutput.toString(), threadDumpBuilder.build());
    }
    
    private void appendPrefix(StringBuilder output)
    {
        output.append(MessageFormat.format(
            "Confluence {0} thread dump taken on {1,date,medium} at {1,time,medium}:\n",
            GeneralUtil.getVersionNumber(), new Date()));
    }
}
